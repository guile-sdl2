;;; guile-sdl2 --- FFI bindings for SDL2
;;; Copyright © 2025 Vivien Kraus <vivien@planete-kraus.eu>
;;;
;;; This file is part of guile-sdl2.
;;;
;;; Guile-sdl2 is free software; you can redistribute it and/or modify
;;; it under the terms of the GNU Lesser General Public License as
;;; published by the Free Software Foundation; either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Guile-sdl2 is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with guile-sdl2.  If not, see
;;; <http://www.gnu.org/licenses/>.

(use-modules (sdl2)
             (sdl2 rect)
             (sdl2 render)
             (sdl2 surface)
             (sdl2 video)
             (rnrs bytevectors))

(define (draw ren)
  ;; This texture uses 4 bytes per pixel.
  (let ((texture (make-texture ren 'rgba8888 'streaming 640 480))
        (red (u8-list->bytevector (list 200 10 20 255)))
        (blue (u8-list->bytevector (list 10 20 180 255))))
    ;; Set everything white
    (call-with-values
        (lambda ()
          (lock-texture! texture #f))
      (lambda (data pitch)
        (bytevector-fill! data 255)))
    (unlock-texture! texture)
    ;; Alternate red and blue lines in the middle
    (call-with-values
        (lambda ()
          (lock-texture! texture (make-rect 50 50 540 380)))
      (lambda (data pitch)
        ;; pitch is how much space it takes to store a row. Since we
        ;; only locked a part of the texture, it will be more than 540
        ;; * 4 bytes, we just have to skip the gap.
        (do ((row-index 0 (+ row-index 1)))
            ((>= row-index 380))
          (let ((color (if (even? row-index) red blue)))
            (do ((column-index 0 (+ column-index 1))
                 (output-index (* row-index pitch) (+ output-index 4)))
                ((>= column-index 540))
              (bytevector-copy! color 0 data output-index 4))))))
    (unlock-texture! texture)
    (clear-renderer ren)
    (render-copy ren texture)
    (present-renderer ren)
    (sleep 2)))

(sdl-init)

(call-with-window (make-window)
  (lambda (window)
    (call-with-renderer (make-renderer window) draw)))

(sdl-quit)
